.PHONY: all clean

all:	accepthack.so

accepthack.so:
	gcc -shared -fPIC -Wall -o accepthack.so accepthack.c

clean:
	rm -f accepthack.so
