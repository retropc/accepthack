#define _GNU_SOURCE
#include <sys/socket.h>
#include <dlfcn.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/un.h>
#include <errno.h>
#include <unistd.h>

#define __LOG(level, format, ...) do { \
  fputs("accepthack: " level " ", stderr); \
  fprintf(stderr, format, ##__VA_ARGS__); \
  fputs("\n", stderr); \
} while(0)

#define DEBUG(format, ...) do { if (debug) { __LOG("DEBUG", format, ##__VA_ARGS__); } } while(0)
#define INFO(format, ...) __LOG("INFO", format, ##__VA_ARGS__)
#define WARN(format, ...) __LOG("WARN", format, ##__VA_ARGS__)
#define FATAL(format, ...) do { __LOG("FATAL", format, ##__VA_ARGS__); abort(); } while(0)

static int (*real_accept)(int, struct sockaddr *, socklen_t *);
static int (*real_accept4)(int, struct sockaddr *, socklen_t *, int);

static int debug;
static char *socket_prefix;
static size_t socket_prefix_len;

static int get_buf_size(void) {
  FILE *f = fopen("/proc/sys/net/core/wmem_max", "r");
  if (!f) {
    FATAL("unable to open wmem_max, errno: %d", errno);
  }

  char buf[512];
  char *r = fgets(buf, sizeof(buf), f);
  fclose(f);

  if (!r) {
    FATAL("error reading wmem_max, errno: %d", errno);
  }

  size_t len = strlen(buf);
  if (!len || buf[len - 1] != '\n') {
    FATAL("error reading wmem_max: (no newline at end)");
  }

  buf[len] = '\0';
  int buf_size = atoi(buf);
  if (buf_size <= 0) {
    FATAL("error converting wmem_max");
  }

  if (buf_size == 212992) {
    WARN("wmem_max is default value: %d", buf_size);
  }

  return buf_size;
}

void __attribute__ ((constructor)) my_init(void) {
  debug = getenv("ACCEPTHACK_DEBUG") != NULL;

  real_accept = dlsym(RTLD_NEXT, "accept");
  if (!real_accept) {
    FATAL("unable to lookup accept");
  }
  real_accept4 = dlsym(RTLD_NEXT, "accept4");
  if (!real_accept4) {
    FATAL("unable to lookup accept4");
  }

  char *socket_prefix_s = getenv("ACCEPTHACK_SOCKET_PREFIX");
  char buf[512];
  if (!socket_prefix_s) {
    snprintf(buf, sizeof(buf), "/run/user/%d/wayland-", getuid());
    socket_prefix_s = buf;
    DEBUG("ACCEPTHACK_SOCKET_PREFIX not set (using default)");
  }

  socket_prefix_len = strlen(socket_prefix_s);
  socket_prefix = malloc(socket_prefix_len + 1);
  if (!socket_prefix) {
    FATAL("unable to allocate");
  }
  memcpy(socket_prefix, socket_prefix_s, socket_prefix_len + 1);

  INFO("loaded, buf_size: %d socket_prefix: %s", get_buf_size(), socket_prefix);
}

static void set_buffer_size(int fd) {
  if (fd < 0) {
    return;
  }

  int type;
  socklen_t type_len = sizeof(type);
  if (getsockopt(fd, SOL_SOCKET, SO_TYPE, &type, &type_len) == -1) {
    WARN("fd: %d unable to lookup socket type, errno: %d", fd, errno);
    return;
  }

  if (type != SOCK_STREAM) {
    DEBUG("fd: %d socket type incorrect (%d)", fd, type);
    return;
  }

  socklen_t addr_len = 1024;
  struct sockaddr *addr = alloca(addr_len);
  if (getsockname(fd, (struct sockaddr *)addr, &addr_len) == -1) {
    WARN("fd: %d unable to lookup socket name, errno: %d", fd, errno);
    return;
  }

  if (addr->sa_family != AF_UNIX) {
    DEBUG("fd: %d socket type not unix: %d", fd, addr->sa_family);
    return;
  }

  struct sockaddr_un *addr_un = (struct sockaddr_un *)addr;
  if (strncmp(addr_un->sun_path, socket_prefix, socket_prefix_len)) {
    DEBUG("fd: %d socket path doesn't match prefix: %s", fd, addr_un->sun_path);
    return;
  }

  int buf_size = get_buf_size();
  int v = buf_size;
  socklen_t v_len = sizeof(v);
  if (setsockopt(fd, SOL_SOCKET, SO_SNDBUF, &v, v_len) == -1) {
    WARN("fd: %d setting send buffer size failed, errno %d", fd, errno);
    return;
  }

  if (getsockopt(fd, SOL_SOCKET, SO_SNDBUF, &v, &v_len) == -1) {
    WARN("fd: %d setting send buffer size failed, errno %d", fd, errno);
    return;
  }

  if (v < buf_size) {
    WARN("fd: %d setting send buffer size didn't apply (new size is: %d)", fd, v);
    return;
  }

  INFO("fd: %d success setting send buffer size", fd);
}

int accept(int sockfd, struct sockaddr *addr, socklen_t *addrlen) {
  int result = real_accept(sockfd, addr, addrlen);
  set_buffer_size(result);
  return result;
}

int accept4(int sockfd, struct sockaddr *addr, socklen_t *addrlen, int flags) {
  int result = real_accept4(sockfd, addr, addrlen, flags);
  set_buffer_size(result);
  return result;
}
